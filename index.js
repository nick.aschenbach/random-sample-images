const Jimp = require("jimp");
const { execSync } = require("child_process");

//ffmpeg -r 300 -i img_%05d.jpg output.mp4

const width = 800;
const height = 800;
const iterations = 10000;

const createImage = async () => {
  return new Promise((resolve, reject) => {
    new Jimp(width, height, (err, image) => {
      if (err) reject(err);

      resolve(image);
    });
  });
};

const save = ({ image, filename, quality }) => {
  return new Promise((resolve, reject) => {
    image.quality(quality);
    image.write(`${filename}`, (err, data) => {
      if (err) reject(err);

      resolve();
    });
  });
};

const createRandomImageSets = async () => {
  execSync("rm *.jpg || echo 'noop'");

  const font = await Jimp.loadFont(Jimp.FONT_SANS_16_WHITE);
  const image = await createImage();
  for (let i = 0; i < iterations; i++) {
    const color = Jimp.rgbaToInt(255, 252, 0, 255);
    image.setPixelColor(color, width * Math.random(), height * Math.random());
    const newImage = image.clone();
    newImage.print(font, 10, 10, `n = ${i + 1}`);

    const filename = `img_${(i).toString().padStart(5, "0")}.jpg`;
    await save({ image: newImage, filename, quality: 80 });
  }
};

createRandomImageSets();
